# drakon

Javascript-based drag & connect graphs.

Create flows with drakon, a drag and connect graph maker, open-source.

I started this project because I wanted to create complex flows, which could be exported into an easy to understand format, and then provided to 3rd party apps for reading and understanding, but almost everything I saw out there had horrible pricing. So why not start it myself?

